# Project Instanotary
This React Native based application aims to give users a way to instantly notarize any data or file they want with current datetime.<br>
Find apk [here](https://gitlab.com/mmitrasish/bluzellenotaryapp/-/releases/).
## Feature List
### App Side Features
1. Login using phone auth with just phone number and verification code.
### Notary
1. Add image directly from camera/gallery, documents like .doc, .pdf, etc, video and text too.
2. Notarize the file with name and description and timestamp.
3. The file is permanently stored in IPFS, User can use the hash from the app to directly lookup their file.
4. Showing preview for viewable file.
5. User can Download both viewable and non-viewable file.
### Codebase
1. This application is purely written in react native thus it can run on both iOS and android without changing a single line of code.
2. Clean codebase for easy modeification and usage.

#### NOTE 1:- This application is throughly tested to be running on android as well as iOS without any change in code, but if you face any issues please reach out to me on telegram `@rekpero` or discord `@rekpero#3898`
#### NOTE 2:- I tried to impliment all the possible checks, but if you encounter any issues or bugs, please reach out to me.

## Demo Video
You can watch the small demo video [here](https://drive.google.com/file/d/1Mm8V093ULBbVRPmZH0JZQ6uhasFEbQXB/view?usp=sharing).

## License
Instanotary is licensed under [the MIT license](https://gitlab.com/mmitrasish/bluzellenotaryapp/-/blob/master/LICENSE).

## Screenshots
<img src="Screenshots/Screenshot1.png" width="400"> <img src="Screenshots/Screenshot2.png" width="400">
<img src="Screenshots/Screenshot3.png" width="400"> <img src="Screenshots/Screenshot4.png" width="400">
<img src="Screenshots/Screenshot5.png" width="400"> <img src="Screenshots/Screenshot6.png" width="400">
<img src="Screenshots/Screenshot7.png" width="400"> <img src="Screenshots/Screenshot8.png" width="400">
- Head over to screenshots folder for more screenshots.
